#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_custom_1(self):
        s = "2 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2)
        self.assertEqual(j, 20)

    def test_read_custom_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)

    def test_read_custom_3(self):
        s = "1000 1001\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1000)
        self.assertEqual(j, 1001)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # Corner Case (0 in range)
    def test_eval_custom_1(self):
        try:  # pragma: no cover
            v = collatz_eval(0, 10)
            self.assertEqual(v, 20)
        except:
            pass
            # print("-- Failed test_eval_custom_1: 0, 10")

    # Corner Case (i is larger than j)
    def test_eval_custom_2(self):
        try:
            v = collatz_eval(10, 1)
            self.assertEqual(v, 20)
        except:  # pragma: no cover
            pass
            # print("-- Failed test_eval_custom_2: 10, 1")

    # Corner Case (negative number)
    def test_eval_custom_3(self):
        try:  # pragma: no cover
            v = collatz_eval(-10, 10)
            self.assertEqual(v, 20)
        except:
            pass
            # print("-- Failed test_eval_custom_3: -10, 10")

    # Corner Case (i == j)
    def test_eval_custom_4(self):
        try:
            v = collatz_eval(1, 1)
            self.assertEqual(v, 1)
        except:  # pragma: no cover
            pass
            # print("-- Failed test_eval_custom_4: 1, 1")

    def test_eval_custom_5(self):
        try:
            v = collatz_eval(5700, 5799)
            self.assertEqual(v, 205)
        except:  # pragma: no cover
            pass
            # print("-- Failed test_eval_custom_5: 5700, 5799")

    def test_eval_custom_6(self):
        v = collatz_eval(27, 479)
        self.assertEqual(v, 144)

    def test_eval_custom_7(self):
        v = collatz_eval(90, 210)
        self.assertEqual(v, 125)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_custom_1(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_custom_2(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")

    def test_print_custom_3(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_custom_1(self):
        r = StringIO("3 92\n5 1000\n120 1203\n321 2102\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3 92 116\n5 1000 179\n120 1203 182\n321 2102 182\n")

    def test_solve_custom_2(self):
        r = StringIO("21 3000\n290 2102\n340 3402\n603 4301\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "21 3000 217\n290 2102 182\n340 3402 217\n603 4301 238\n")

    def test_solve_custom_3(self):
        r = StringIO("782 5102\n2192 2230\n333 32010\n3223 45000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "782 5102 238\n2192 2230 183\n333 32010 308\n3223 45000 324\n")


# ----
# main
# ----
if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
